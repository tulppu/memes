"""memes URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
import memes.controller
from django.urls import path
import memes.lib.files

urlpatterns = [
    # url(r'^admin/', admin.site.urls),
    path('', memes.controller.index),
    path('randoms/', memes.controller.index, {'randoms': True}),
    path('most_posted/', memes.controller.most_posted),

    path('threads/', memes.controller.threads),

    path('stats/', memes.controller.stats),

    path('posts/', memes.controller.posts, {'source': None}),
    path('posts/randoms/', memes.controller.posts, {'source': None}),
    path('posts/<str:source>/', memes.controller.posts),
    path('posts/<str:source>/<str:board_name>/', memes.controller.posts),
    path('posts/<str:source>/<str:board_name>/<int:thread_no>/', memes.controller.posts),

    path('<int:file_id>/', memes.controller.file_handler),
    path('<int:file_id>/show_similar', memes.controller.file_handler, {'show_similar': True}),
    path('<int:file_id>/player/', memes.controller.file_player),

    path('monitors/<str:monitor_name>/toggle/', memes.controller.toggle_monitor),

    # By file type
    path('videos/', memes.controller.index, {'file_type': memes.lib.files.FILE_TYPE_VID}),
    path('pictures/', memes.controller.index, {'file_type': memes.lib.files.FILE_TYPE_PIC}),
    path('audios/', memes.controller.index, {'file_type': memes.lib.files.FILE_TYPE_AUDIO}),

    path('videos/randoms/', memes.controller.index, {'file_type': memes.lib.files.FILE_TYPE_VID, 'randoms': True}),
    path('pictures/randoms/', memes.controller.index, {'file_type': memes.lib.files.FILE_TYPE_PIC, 'randoms': True}),
    path('audios/randoms/', memes.controller.index, {'file_type': memes.lib.files.FILE_TYPE_AUDIO, 'randoms': True}),

    path('<str:source>/pictures/', memes.controller.sourced, {'file_type': memes.lib.files.FILE_TYPE_PIC}),
    path('<str:source>/videos/', memes.controller.sourced, {'file_type': memes.lib.files.FILE_TYPE_VID}),
    path('<str:source>/audios/', memes.controller.sourced, {'file_type': memes.lib.files.FILE_TYPE_AUDIO}),

    path('<str:source>/<str:board_name>/pictures/',
         memes.controller.sourced, {'file_type': memes.lib.files.FILE_TYPE_PIC}),

    path('<str:source>/<str:board_name>/videos/', memes.controller.sourced,
         {'file_type': memes.lib.files.FILE_TYPE_VID}),
    path('<str:source>/?<str:board_name>/audios/', memes.controller.sourced,
         {'file_type': memes.lib.files.FILE_TYPE_AUDIO}),


    # General source/board
    path('<str:source>/', memes.controller.sourced),
    path('<str:source>/randoms/', memes.controller.sourced, {'randoms': True}),
    path('<str:source>/<str:board_name>/', memes.controller.sourced),
    path('<str:source>/<str:board_name>/randoms/', memes.controller.sourced, {'randoms': True}),

    path('<str:source>/<str:board_name>/<int:thread_no>/', memes.controller.sourced),


]