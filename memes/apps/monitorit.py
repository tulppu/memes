from django.apps import AppConfig

class MonitoritConfig(AppConfig):


    name = "memes.apps.monitorit"

    def ready(self):

        import memes.lib.utils
        if not memes.lib.utils.get_config().getboolean("app", "auto_start_monitors"):
            return

        import memes.monitors.monitor_manager
        for monitor in memes.monitors.monitor_manager.monitors:
            if monitor.auto_startup is True:
                memes.monitors.monitor_manager.start_monitor(monitor)
