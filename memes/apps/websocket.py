from django.apps import AppConfig
from django.template import loader
import websockets
from threading import Thread

subs = {}

import concurrent.futures
pool = concurrent.futures.ThreadPoolExecutor()

async def on_message(client, path):
    channel = await client.recv()
    print("channel: " + channel)

    listeners = subs[channel] if channel in subs else []
    listeners.append(client)

    subs[channel] = listeners
    print(listeners)
    while True:
        try:
            await client.recv()
        except websockets.ConnectionClosed:
            print(f"Terminated")
            subs[channel].remove(client)
            break

from django.dispatch import receiver
import memes.signals

import asyncio

@receiver(memes.signals.post_fully_built)
def new_post(post, **kwargs):

    template = loader.get_template('post.html')
    context = {
        "post": post,
    }
    html = template.render(context)

    async def broadcast_post(instance):

        source = instance.thread.source.lower()
        board = instance.thread.board
        thread_no = instance.thread.no

        channels = [
                "/posts/",
                "/posts/" + source + "/",
                "/posts/" + source + "/" + board + "/",
                "/posts/" + source + "/" + board + "/" + str(thread_no) + "/"
        ]

        for c in channels:
            listeners = subs[c] if c in subs else []
            for client in listeners:
                await client.send(html)
    #await broadcast_post(post)
    # import memes.monitors.monitor_manager
    monitor = memes.monitors.monitor_manager.get_monitor_by_name(post.thread.source)
    monitor.loop.run_until_complete(broadcast_post(post))

async def echo_server(stop):
    print("avataan")
    async with websockets.serve(on_message, "localhost", 8081):
        await stop


def server_looper(loop,stop):
    import asyncio
    asyncio.set_event_loop(loop)
    loop.run_until_complete(echo_server(stop))
    asyncio.get_event_loop().run_forever()

def handle_server():
    import asyncio

    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    stop = loop.create_future()

    def stop_loop(res):
        #import sys
        #sys.exit(0)

        stop.set_result(0)
        #loop.stop()

    #loop.add_signal_handler(signal.SIGTERM, stop_loop, None)
    #loop.add_signal_handler(signal.SIGINT, stop_loop, None)

    t = Thread(daemon=True, target=server_looper, args=(loop,stop))
    t.start()



class WebsocketConfig(AppConfig):

    name = "memes.apps.websocket"

    def ready(self):

        import memes.monitors.monitor_manager
        monitor_manager = memes.monitors.monitor_manager
        print("Starting server")
        from threading import Thread
        t = Thread(target=handle_server, daemon=True)
        t.start()
        #handle_server()

        print("Started socket server")

