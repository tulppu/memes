import websockets
import asyncio


class Server:

    server = None

    def __init__(self):
        if not self.server:
            return
            self.server = websockets.serve(None, "127.0.0.1", 8081)

            asyncio.get_event_loop().run_until_complete(self.server)

    def broadcast(self, message):
        return
        if self.server:
            for client in self.server.connections.iterValues():
                client.sendMessage(message)
