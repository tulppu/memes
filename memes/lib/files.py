import memes.models
import magic
import imagehash
import uuid
import mimetypes
import hashlib
from PIL import Image
import shutil
import requests
import subprocess
from time import sleep
import os
import memes.lib.utils
from pathlib import Path
import time


FILE_TYPE_PIC = 'PICTURE'
FILE_TYPE_VID = 'VIDEO'
FILE_TYPE_AUDIO = 'AUDIO'

PIC_MIMES = [
    "image/jpg", "image/png", "image/webp"
]

SUPPORTED_FILES = {
    FILE_TYPE_PIC: [
        "image/jpeg", "image/png", "image/gif", "image/webp",
    ],
    FILE_TYPE_VID: [
        "video/mp4", "video/webm", "video/quicktime", "video/x-m4v", "video/3gpp", "video/x-matroska"
    ],
    FILE_TYPE_AUDIO: [
        "AUDIO", "audio/webm", "audio/mpeg", "audio/mp3", "audio/x-matroska",
    ]
}


def get_file_type(mime):
    for file_type in SUPPORTED_FILES:
        for i in SUPPORTED_FILES[file_type]:
            if i == mime:
                return file_type


def has_enough_space(storage):
    free_mbs = shutil.disk_usage(storage.file_path).free / 1000000

    required_space = memes.lib.utils.get_config().getint("app", "required_reserved_space")

    return free_mbs > required_space


def remove_till_enough_space(storage, delete_count = 0):
    if has_enough_space(storage):
        return delete_count

    # TODO: take file size in account?
    file = memes.models.File.objects.filter(content_downloaded=1).order_by('added_at').first()

    if not file:
        raise Exception("More free space is needed and no files were found to be deleted.")

    file_path = file.cached_storage.file_path + file.file_name

    if os.path.isfile(file_path):
        try:
            os.unlink(file_path)
        except OSError:
            pass
        print("File " + str(file.id) + " wad deleted from system")
    else:
        print("File " + file_path + " for object " + str(file.id) + " didn't exists in system")

    file.content_downloaded = 0
    file.save()

    return remove_till_enough_space(storage, delete_count)


def get_video_duration(input_video):
    command = 'ffprobe -i "' + input_video + '" -show_entries format=duration -v quiet -of csv="p=0"'
    result = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)
    output = result.communicate()
    return output[0]


def get_video_phash(input_video):
    output = "/tmp/" + str(uuid.uuid4()) + ".jpg"
    # command = "ffmpegthumbnailer -i " + input_video + " -t 1% -q 10 -a -o " + output
    # os.system(command)

    generate_thumb(input_video, output, FILE_TYPE_VID)

    if not os.path.isfile(output):
        raise Exception('Failed to generate thumbnail from ' + input_video + ' for phashing')
    img = Image.open(output)
    os.unlink(output)

    return imagehash.phash(img)


def add_file(path, storage=None):
    file = memes.models.File()

    if not storage:
        storage = memes.models.Storage.objects.all().first()

    if not storage:
        storage = memes.models.Storage()
        storage.file_path = os.path.dirname(os.path.realpath(__file__)) + "/../../static/"
        storage.save()

    if not storage.file_path:
        raise Exception('Storage path is not set')

    if not has_enough_space(storage):
        remove_till_enough_space(storage)

    file.mime = magic.from_file(path, True)

    if not file.mime:
        print("Mime could not be found for " + path)
        return

    file.storage = storage
    file.size = os.path.getsize(path)

    ext = mimetypes.guess_extension(file.mime, False)

    if not ext:
        print("No ext was found for file " + path)

    ext = ".jpg" if ext == ".jpe" else ext
    ext = ".mp4" if file.mime == "video/x-m4v" else ext

    file.file_name = str(int(time.time())) + "-" + str(uuid.uuid4())[0:2] + str(ext)

    file.md5 = hashlib.md5(open(path, 'rb').read()).hexdigest()
    file.type = get_file_type(file.mime)
    file.content_downloaded = 1

    if not file.type:
        print("File " + path + " has not supported mime ("+ file.mime + " )")
        return

    if file.type is FILE_TYPE_PIC:
        Image.LOAD_TRUNCATED_IMAGES = True
        try:
            img = Image.open(path)
            file.phash = imagehash.phash(img)
            img.close()
        except Exception as e:
            print(file.file_name + " could not be phashed")
            print(str(e))
            pass

    if file.type is FILE_TYPE_VID:
        try:
            get_video_duration(path)
            file.phash = get_video_phash(path)
        except Exception as e:
            print("Failed to get video duration or phash")
            print(str(e))
            pass

    new_path = storage.file_path + file.file_name

    shutil.move(path, new_path)
    file.save()

    return file


def download_to_path(url, target_path, http_session, retry_count=0):
    print("Downloading " + url)
    #sleep(0.5)
    try:
        res = http_session.get(url, timeout=10, verify=False)
    except (requests.exceptions.Timeout, requests.exceptions.ConnectionError, requests.exceptions.ChunkedEncodingError):
        if retry_count is 5:
            print("Failed to download file " + url)
            return None

        sleep(2+retry_count)
        return download_to_path(url, target_path, http_session, retry_count + 1)
    content = res.content if res else None
    if not content or not len(content) or not res.ok:
        return False

    with open(target_path, 'wb') as out:
        out.write(content)
        out.close()

    return content


def generate_thumb(source_path, outpath, type):
    if not len(type):
        raise Exception("Source type is not specified")

    if type != FILE_TYPE_PIC and type != FILE_TYPE_VID:
        print("File type {} is not supported".format(type))
        return False

    if type == FILE_TYPE_PIC:

        #size = Path(source_path).stat().st_size / 1024 / 1024
        #if size > 20:
        #    return False

        try:
            img = Image.open(source_path) #.convert("RGB")

            if img.mode in ("RGBA", "P"):
               img = img.convert("RGB")

            img.thumbnail([80, 80])
            img.save(outpath, quality=20)
            img.close()
        except IOError as e:
            print("Failed to thumb " + source_path)
            print(str(e))
    if type == FILE_TYPE_VID:
        command = "ffmpeg -i " + source_path + " -y -ss 0 -vf scale='80:80:force_original_aspect_ratio=decrease' -q 2 " + outpath
        #command = "ffmpegthumbnailer -i " + source_path + " -t 1% -q 7 -s 80 -o " + outpath
        result = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)
        result.communicate()

    if not os.path.isfile(outpath) or os.path.getsize(outpath) == 0:
        print('Failed to generate thumbnail from ' + source_path)
        return False

    return True
