import calendar
from datetime import datetime
import time
import configparser
import re

config = None


def get_config():
    global config
    if not  config:
        config = configparser.ConfigParser()
        config.sections()

        config.read("config.ini")

    return config


def get_current_unix():
    return time.time()


def str_date_to_unix(date, timezone, format="%d.%m.%Y %H:%M:%S"):
    d = datetime.strptime(date, format)
    return calendar.timegm(d.utctimetuple())


def remove_emojii(text):
    # https://stackoverflow.com/a/33417311
    emoji_pattern = re.compile("["
                               u"\U0001F600-\U0001F64F"  # emoticons
                               u"\U0001F300-\U0001F5FF"  # symbols & pictographs
                               u"\U0001F680-\U0001F6FF"  # transport & map symbols
                               u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                               "]+", flags=re.UNICODE)
    return emoji_pattern.sub(r'', text) # no emoji


import html2text
h = html2text.HTML2Text()
h.ignore_emphasis = True
h.links_each_paragraph = True



reply_pattern = re.compile(r'&gt;&gt;(\d+)')

def escape_html(text, convert_html=False, post=None):

    if convert_html:
        text = h.handle(text)
        text = text.strip()
    else:
        text = text.replace("<br>", "\r\n")
        text = text.replace('>', '&gt;')
        text = text.replace('<', '&lt;')

    text = text.replace('\n', '<br>')

    if post:
        text = re.sub(reply_pattern, r'<a class="reply" href="#' + post.thread.source +'_' + post.thread.board + '_\\1">&gt;&gt;\\1</a>', text)

    return text


