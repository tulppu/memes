from django.db import models
from memes.monitors.monitor_manager import monitors, get_monitor_by_name
from os import path
from django.db.models.signals import pre_save, post_save
import time
from memes.lib.utils import escape_html



class Storage(models.Model):
    file_path = models.CharField(max_length=254)
    uri = models.CharField(max_length=254)
    list = {}



class File(models.Model):
    storage = models.ForeignKey(Storage, on_delete=models.PROTECT)
    file_name = models.CharField(max_length=254)
    size = models.BigIntegerField()
    mime = models.CharField(max_length=32)
    duration = models.PositiveIntegerField(blank=True, null=True)
    md5 = models.CharField(max_length=254, db_index=True, unique=True)
    phash = models.CharField(max_length=254, blank=True, db_index=True)
    type = models.CharField(max_length=16, db_index=True)
    content_downloaded = models.SmallIntegerField(null=True, default=1, db_index=True)
    added_at = models.DateTimeField(auto_now_add=True, null=False)
    post_count = models.PositiveIntegerField(default=0, db_index=True)

    def get_storage(self):
        if self.storage_id in Storage.list:
            return Storage.list[self.storage_id]
        storage =  Storage.objects.filter(id=self.storage_id).first()
        Storage.list[self.storage_id] = storage
        return storage

    @property
    def cached_storage(self):
        return self.get_storage()


    def get_uri(self):
        if not self.content_downloaded:
            if not path.isfile(self.get_path()):
                return self.get_orig_uri()
            return None

        storage = File.get_storage(self)
        if storage and storage.uri:
            return storage.uri + self.file_name
        return "/static/medias/" + str(self.file_name)

    def get_thumb_uri(self):
        storage = File.get_storage(self)
        if storage and storage.uri:
            return storage.uri + "thumbs/" + str(self.id) + ".jpg"
        return "/static/medias/thumbs/" + str(self.id) + ".jpg"

    def get_post_count(self):
        return PostFile.objects.filter(file=self).count()

    def get_orig_uri(self):
        post_file = PostFile.objects.filter(post__thread__is_locked=False, file=self).first()
        return post_file.orig_url if post_file else None

    def get_path(self):
        return self.cached_storage.file_path + self.file_name

class Thread(models.Model):
    no = models.BigIntegerField(db_index=True)
    subject = models.CharField(max_length=512, null=True)
    timestamp = models.PositiveIntegerField(null=False)
    board = models.CharField(max_length=16, db_index=True)
    source = models.CharField(max_length=32, db_index=True)
    last_check_time = models.PositiveIntegerField(default=0)
    is_locked = models.BooleanField(default=False)
    added_at = models.DateTimeField(auto_now_add=True, null=True)

    class Meta:
        unique_together = ('source', 'no', 'board')

    def get_orig_uri(self):
        monitor = get_monitor_by_name(self.source)

        if not monitor:
            #raise Exception("Monitor '{}' was not found".format(self.source))
            return None

        return monitor.get_thread_url(board=self.board, no=self.no)

    def get_op_post(self):
        post = Post.objects.filter(thread_id=self).first()
        print(self.id, post)
        return post

    def get_op_post_file(self):
        res = File.objects.filter(post_file__post__thread=self).first()
        return res

def thread_pre_save(sender, instance, **kwargs):
    instance.last_check_time = int(time.time())
    if instance.subject:
        monitor = get_monitor_by_name(instance.source)
        instance.subject = escape_html(text=instance.subject)
        instance.subject = instance.subject[:512]

pre_save.connect(thread_pre_save, sender=Thread)


class Post(models.Model):
    no = models.PositiveIntegerField(db_index=True)
    thread = models.ForeignKey(Thread, on_delete=models.PROTECT, db_index=True, related_name='posts')
    country = models.CharField(max_length=8, null=True)
    message = models.CharField(max_length=10000, null=True)
    timestamp = models.PositiveIntegerField(null=True, default=0)
    added_at = models.DateTimeField(auto_now_add=True, null=True)
    op = models.PositiveIntegerField(1, null=True)

    class Meta:
        unique_together = ('no', 'thread')


def post_pre_save(sender, instance, **kwargs):
    if instance.message:
        thread = Thread.objects.filter(id=instance.thread.id).first()
        monitor = get_monitor_by_name(thread.source)

        instance.message = escape_html(instance.message, convert_html=monitor.html_to_text, post=instance)
        instance.message = instance.message[:10000]



pre_save.connect(post_pre_save, sender=Post)

class PostFile(models.Model):
    post = models.ForeignKey(Post, on_delete=models.PROTECT, related_name='post_files', db_index=True)
    file = models.ForeignKey(File, on_delete=models.PROTECT, db_index=True, related_name='post_file')
    file_name = models.CharField(max_length=254)
    orig_url = models.CharField(max_length=1024, default=None)


    class Meta:
        unique_together = ('post', 'file')

def post_file_pre_save(sender, instance, **kwargs):
    if instance.file_name:
        instance.file_name = escape_html(instance.file_name)
        instance.file_name = instance.file_name[:254]
