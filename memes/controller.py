from django.http import HttpResponse
from django.template import loader
import memes.models
from django.shortcuts import get_object_or_404
import memes.monitors.monitor_manager
from django.http import Http404
from django.shortcuts import redirect
from django.db.models import Q
import datetime
from django.db.models import Sum, prefetch_related_objects, Subquery
import time

def index(request, file_type=False, most_posted=False, randoms=False):
    q = Q()

    if file_type:
        q &= Q(type=file_type)

    if randoms:
        q &= Q(content_downloaded=True);

    order = "-post_count" if most_posted else "?" if randoms else "-id"
    files = memes.models.File.objects.filter(q).order_by(order)


    template = loader.get_template('index.html')
    context = {
        "files": files[:350],
        "source": '',
        "board_name": '',
        "boards": '',
        "type": 'randoms/' if randoms else '',
        "monitors": memes.monitors.monitor_manager.monitors,
        "file_type": file_type,
    }
    
    return HttpResponse(template.render(context, request))



def sourced(request, source=None, board_name=None, thread_no=None, file_type=None, randoms=False):
    boards = []

    file_name_search = request.GET.get("q", None)
    posts_q = Q()
    q = Q()

    if not file_name_search:
        q = Q(content_downloaded=True)

    if file_type:
        q = Q(type=file_type, content_downloaded=True)
        print(file_type)


    if source:
        monitor = memes.monitors.monitor_manager.get_monitor_by_name(source, True)
        boards = monitor.boards if monitor else []

        # TODO: confirm working
        posts_q &= Q(post__thread__source=monitor.name)

    if board_name:
        posts_q &= Q(post__thread__board=board_name)

    if thread_no:
        posts_q &= Q(post__thread__no=thread_no)

    if file_name_search:
        posts_q &= Q(file_name__icontains=file_name_search)

    postsfiles_subquery = memes.models.PostFile.objects.filter(posts_q).values('file_id')

    order_by = "?" if randoms else "-added_at"
    files = memes.models.File.objects.filter(q, id__in=Subquery(postsfiles_subquery)).select_related("storage").order_by(order_by)[:150]

    template = loader.get_template("index.html")

    if source:
        source += "/"
    if board_name:
        board_name += "/"

    context = {
        "files": files,
        "type": '',
        "view": '',
        "monitors": memes.monitors.monitor_manager.monitors,
        "source": source,
        "board_name": board_name or '',
        "boards": boards,
    }
    return HttpResponse(template.render(context, request))


def most_posted(request):
    files = memes.models.File.objects.order_by("-post_count")[:300]
    template = loader.get_template("index.html")

    context = {
        "files": files,
        "monitors": memes.monitors.monitor_manager.monitors,
    }

    return HttpResponse(template.render(context, request))




def posts(request, source, board_name=None, thread_no=None):

    filter = request.GET.get("q", None)
    op_search_only = request.GET.get("op_only", False)
    country = request.GET.get("country", False);

    q = Q()
    if not op_search_only:
        q &= Q(thread__is_locked=False)
    if country:
        q &= Q(country=country)
    
    monitor = None
    if source:
        monitor = memes.monitors.monitor_manager.get_monitor_by_name(source, True)

    boards = []
    if source:
        q &= Q(thread__source=source)
        boards = monitor.boards if monitor else []


    if board_name:
        q &= Q(thread__board=board_name)

    if op_search_only:
       q &= Q(op=1)


    order = "-id"

    thread = None
    if thread_no:
        thread = memes.models.Thread.objects.filter(source=source, board=board_name, no=thread_no).first()
        # override previous
        q = Q(thread=thread)
        order = "no"

    if filter:
        q &= (Q(message__icontains=filter) | Q(thread__subject__icontains=filter))


    posts = memes.models.Post.objects.filter(q).select_related("thread").order_by(order)\
        .prefetch_related("post_files", "post_files__file") 

    if source:
        source += "/"
    if board_name:
        board_name += "/"
   
    context = {
        "view": "posts/",
        "source": source or '',
        "board_name": board_name or '',
        
        "posts": posts[:350],
        "monitor": monitor,
        "boards": boards,
        "monitors": memes.monitors.monitor_manager.monitors,
        "thread": thread,
        "q": filter if filter else ""
    }

    if thread:
        ts = time.time();
        diff_type = "minutes"
        ts_diff = round((ts - thread.last_check_time) / (60));

        if ts_diff > 60:
            ts_diff = round((ts - thread.last_check_time) / (60 * 60), 1);
            diff_type = "hours"
        if ts_diff > 24:
            diff_type = "days"
            ts_diff = round((ts - thread.last_check_time) / (60 * 60 * 24), 1) 
            
        context["thread_last_fetch_ago"] = "Checked " + str(ts_diff) + " " + diff_type + " ago" 

    template = loader.get_template('posts.html')
    return HttpResponse(template.render(context, request))



def threads(request, source=None, board_name=None, thread_no=None, file_type=None, most_posted=False, randoms=False):
    q = Q()

    q &= (Q(is_locked=False))
    q &~ (Q(posts = None))

    threads = memes.models.Thread.objects.filter(q).order_by("-id")

    template = loader.get_template("threads.html")

    context = {
        "threads": threads[:150],
        "monitors": memes.monitors.monitor_manager.monitors,
        "monitor":  memes.monitors.monitor_manager.get_monitor_by_name(source, True),
        "source": source,
        "board_name": board_name,
    }
    return HttpResponse(template.render(context, request))




def stats(request):

    date_from = datetime.datetime.now() - datetime.timedelta(days=1)

    template = loader.get_template('stats.html')
    context = {
        "monitors": memes.monitors.monitor_manager.monitors,

        "active_files_count": memes.models.File.objects.filter(content_downloaded=True).count(),
        "active_threads_count": memes.models.Thread.objects.filter(is_locked=False).count(),

        "total_files_count": memes.models.File.objects.count(),
        "total_threads_count": memes.models.Thread.objects.count(),
        "total_posts_count": memes.models.Post.objects.count(),

        "fresh_files_added_last_24h": memes.models.File.objects.filter(added_at__gte=date_from).count(),

        "files_active_size": round(memes.models.File.objects.filter(content_downloaded=True).aggregate(summa=Sum('size'))["summa"] / 1000000),
        "files_total_size": round(memes.models.File.objects.all().aggregate(summa=Sum('size'))["summa"] / 1000000),
        "files_last_24h_size": round(memes.models.File.objects.filter(added_at__gte=date_from).aggregate(summa=Sum('size'))["summa"] / 1000000),
    }

    return HttpResponse(template.render(context, request))


def file_handler(request, file_id, show_similar=False):

    file = get_object_or_404(memes.models.File, id=file_id)
    post_files = memes.models.PostFile.objects.filter(file=file)

    thread = post_files.first().post.thread
    thread_posts = memes.models.Post.objects.select_related("post").filter(thread=thread).prefetch_related("post__thread")

    similar_files = None
    if file.phash and show_similar:
        phash = "BIT_COUNT( CAST(CONV(%s, 16, 10) AS UNSIGNED) ^" \
                "CAST(CONV(phash, 16, 10) AS UNSIGNED) )"
        sql = "SELECT * FROM memes.memes_file WHERE " + phash + " <= 5 AND id <> %s"
        similar_files = memes.models.File.objects.raw(sql, [file.phash, file.id])

    template = loader.get_template('file.html')
    context = {
        "file": file,
        "post_files": post_files,
        "thread_posts": thread_posts,
        "show_similar": show_similar,
        "similar_files": similar_files,
        "monitors": memes.monitors.monitor_manager.monitors
    }

    return HttpResponse(template.render(context, request))


def file_player(request, file_id):
    file = get_object_or_404(memes.models.File, id=file_id)

    template = loader.get_template('player.html')
    context = {
        "file": file
    }

    return HttpResponse(template.render(context, request))


def toggle_monitor(request, monitor_name):
    monitor = memes.monitors.monitor_manager.get_monitor_by_name(monitor_name)

    if not monitor:
        raise Http404

    if not monitor.is_running():
        memes.monitors.monitor_manager.start_monitor(monitor)
    else:
        memes.monitors.monitor_manager.stop_monitor(monitor)

    return redirect("/")

