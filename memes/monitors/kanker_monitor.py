import memes.models
import memes.lib.files
import os
import memes.lib.files
import memes.monitors.base_monitor
import memes.lib.utils
from time import sleep
import requests.exceptions
from memes.signals import post_fully_built

class KankerMonitor(memes.monitors.base_monitor.BaseMonitor):

    url = "https://4chan.org/"
    name = '4chan'
    boards = ['b', 'pol', 'int', 'his', 'r9k', 'bant', 'h']
    time_zone = 'America/New_York'
    required_post_age = 60 * 60
    html_to_text = True
    auto_startup = False


    def get_catalog_url(self, board):
        return "http://a.4cdn.org/" + board + "/catalog.json"

    def fetch_catalog(self):

        for board in self.boards:

            last_fetch = self.board_last_fetch[board] if board in self.board_last_fetch else None

            uri = self.get_catalog_url(board)
            res = self.get_json(uri, last_fetch)
            self.board_last_fetch[board] = memes.lib.utils.get_current_unix()

            if not res:
                self.print("Failed to fetch " + uri)
                continue

            threads = map(lambda x: x["threads"], res)

            for thread in threads:
                thread = thread[0]

                already_added = next(
                    (t for t in self.monitored_threads if t.no == thread["no"] and t.board == board),
                    None
                )

                if already_added:
                    continue

                obj = self.get_thread(thread["no"], board)

                if not obj:
                    obj = memes.models.Thread()
                    obj.no = thread["no"]
                    obj.board = board
                    obj.source = self.name
                    obj.save()

                if obj not in self.monitored_threads:
                    self.monitored_threads.append(obj)

                if len(self.monitored_threads) > 250 * len(self.boards):
                    self.monitored_threads.pop()

    def get_thread_url(self, board, no):
        return "https://boards.4chan.org/{}/thread/{}".format(board, no)

    def fetch_thread(self, thread):
        return self.get_json("http://a.4cdn.org/" + thread.board + "/thread/" + str(thread.no) + ".json", last_modified=thread.last_check_time)

    def syck(self):

        last_catalog_fetch = 0

        while self.thread_status == 1:
            if memes.lib.utils.get_current_unix() > last_catalog_fetch + 60 * 5:
                if not self.get_alive_thread_count() > len(self.boards) * 500:
                    self.fetch_catalog()
                    last_catalog_fetch = memes.lib.utils.get_current_unix()

            thread = self.get_next_thread()

            if not thread:
                self.print("Thread not found to be fetched")
                sleep(60)
                continue

            try:
                thread_data = self.fetch_thread(thread)
            except requests.exceptions.ConnectionError:
                print("Connection error")
                continue
            finally:
                thread.last_check_time = memes.lib.utils.get_current_unix()
                thread.save()

            if thread_data is None:
                #continue
                self.print("Closing thread {} @ {}/{}".format(thread.no, self.name, thread.board))
                thread.is_locked = True
                thread.save()
                continue

            if thread_data is False:
                continue

            posts = thread_data["posts"]


            if posts:
                if not thread.timestamp:
                    thread.timestamp = posts[0]["time"]
                    thread.subject = posts[0]["sub"] if "sub" in posts[0] else None
                    thread.save()
                for post in posts:

                    if self.thread_status == -1:
                        break

                    if self.required_post_age and post["time"] + self.required_post_age > self.get_unix_timestamp():
                        continue

                    if self.post_handler_check_if_added(post, thread):
                        continue

                    self.post_handler(post, thread)


    def get_file_url(self, thread, file_name, post):
        return "http://i.4cdn.org/" + thread.board + "/" + file_name

    def get_thumb_url(self, thread, tim, post):
        return "http://t.4cdn.org/" + thread.board + "/" + tim + "s.jpg"

    def download_thumb(self, thumb_path, tim, thread, post):
        if os.path.isfile(thumb_path):
            os.unlink(thumb_path)
        thumb_url = self.get_thumb_url(thread, tim, post)
        return memes.lib.files.download_to_path(thumb_url, thumb_path, http_session=self.http_session)

    def post_handler_check_if_added(self, post, thread, num_key_name="no"):
        return super(KankerMonitor, self).post_handler_check_if_added(post, thread, num_key_name)


    def post_handler(self, post_data, thread):

        tim = str(post_data["tim"]) if "tim" in post_data else None

        if not tim and not self.archive_postless:
            return None

        file_name = tim + post_data["ext"]
        file_url = self.get_file_url(thread, file_name, post_data)

        file = self.make_file(tim, file_url, post_data, thread)

        if not file and not self.archive_postless:
            return None

        post = memes.models.Post()

        post.thread = thread
        post.no = post_data["no"] if "no" in post_data else None
        post.message = post_data["com_nomarkup"] if "com_nomarkup" in post_data \
            else post_data["com"] if "com" in post_data else None
        post.country = post_data["country"] if "country" in post_data else None
        post.timestamp = post_data["time"] if "time" in post_data else 0

        if post.no == thread.no:
            post.op = 1

        post.save()

        if file:
            post_file = memes.models.PostFile()
            file_name = post_data["filename"] if "filename" in post_data else "unset"
            post_file.file_name = file_name + post_data["ext"]
            post_file.file = file
            post_file.post = post
            post_file.orig_url = file_url

            post_file.save()

        post_fully_built.send_robust(sender=memes.models.Post, post=post)

        return post