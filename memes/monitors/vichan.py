import memes.monitors.base_monitor
import memes.monitors.kanker_monitor
import memes.lib.utils
import os
import memes.lib.files
from memes.signals import post_fully_built

# Base class for chans that are based on vichan.
class ViMonitor(memes.monitors.kanker_monitor.KankerMonitor):

    name = None
    url = None
    html_to_text = True
    auto_startup = False
    required_post_age = 60 * 60
    boards = []

    default_ext = ".jpg"


    def get_catalog_url(self, board):
        return "{}{}/catalog.json".format(self.url, board)

    def get_threads_url(self, board):
        return self.url + board + "/catalog.json"

    def get_thread_url(self, board, no):
        return "{}{}/res/{}.html".format(self.url, board, no)

    def fetch_thread(self, thread):
        return self.get_json(self.url + thread.board + "/res/" + str(thread.no) + ".json", thread.last_check_time-3)

    def get_file_url(self, thread, file_name, post):
        return self.url + thread.board + "/src/" + file_name

    def get_thumb_url(self, thread, tim, post):
        return self.url + thread.board + "/src/" + tim + "s.jpg"

    def get_thumb_ext(self, ext):
        # For video files vichan seems to generate always .jpg even if default ext was set to .png or .gif
        return ".jpg" if ext in [".webm", ".mp4"] else self.default_ext

    def download_thumb(self, thumb_path, tim, thread, post):
        res = super(ViMonitor, self).download_thumb(thumb_path, tim, thread, post)
        # Sometimes (for videos?) thumb is .jpg even if default ext for files seems to be different.
        # Dunno if it's determined by file's size or if .jpg(?) is used as fallback mode.
        if not os.path.isfile(thumb_path):
            thumb_url = self.get_thumb_url(thread, tim, post)[:-4] + post["ext"]
            self.print("Failed to fetch " + self.get_thumb_url(thread=thread, tim=tim, post=post) + ", fetching " + thumb_url + " instead.")
            res =  memes.lib.files.download_to_path(thumb_url, thumb_path, http_session=self.http_session)
        return res

    def post_handler(self, post_data, thread):

        post = super(ViMonitor, self).post_handler(post_data, thread)

        if not post:
            return None

        if not "extra_files" in post_data:
            return post


        for extra_file in post_data["extra_files"]:
            tim = extra_file["tim"]

            file_name = extra_file["filename"] + extra_file["ext"]
            file_url = self.get_file_url(thread, file_name, post_data)

            file = super(ViMonitor, self).make_file(tim, file_url, post_data, thread)

            if file:
                post_file = memes.models.PostFile()
                post_file.file_name = file_name
                post_file.file = file
                post_file.post = post
                post_file.orig_url = file_url

                post_file.save()

        post_fully_built.send_robust(sender=memes.models.Post, post=post)
        return post
