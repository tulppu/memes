from time import sleep
import requests
from multiprocessing import Process
import memes.models
from datetime import datetime
import memes.lib.utils
import hashlib
import shutil
import os
import memes.lib.files
from time import gmtime, strftime
import uuid
from django.db.models.functions import Now
from django.utils import timezone

import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

class BaseMonitor:

    url = None
    name = ''
    process = None
    base_url = None
    boards = []
    handled_posts = []
    monitored_threads = []
    time_zone = None
    # How old in seconds post should be so it can be downloaded.
    required_post_age = 60 * 15
    html_to_text = False
    auto_startup = False
    archive_postless = False
    verify_cert = True
    http_session = requests.Session()

    board_last_fetch = {}


    # -1 = shutting down | 0 = not running | 1 = running
    thread_status = 0
    my_thread = None
    loop = None

    def get_status(self):
        return self.thread_status

    def get_status_string(self):
        if self.thread_status == -1:
            return "offing"
        if self.thread_status == 0:
            return "off"
        if self.thread_status == 1:
            return "on"
        return "invalid"

    def stop_running(self):
        self.thread_status = -1

    def get_json(self, uri, last_modified=None):

        #print("Sending request to " + uri)

        headers = {}

        if last_modified:
            # Wed, 21 Oct 2015 07:28:00 GMT
            string = datetime.fromtimestamp(last_modified - (self.required_post_age + (60 * 60 * 15))).strftime("%a, %d %b %Y %H:%M:%S GMT")
            headers = {'If-Modified-Since': string}
            #print(headers)
        res = None
        try:
            res = self.http_session.get(uri, headers=headers, timeout=5, allow_redirects=False, verify=self.verify_cert)
        #except InsecureRequestWarning as e:
        #    pass
        except Exception as e:
            self.log(str(e))
            return False
            #print(uri + " had a expection.")

        #self.print("[" + str(res.status_code) + "] " + uri)

        if res.status_code in [404, 301, 303]:
            return None

        if not res or not res.content:
            return False

        if not res.content:
            return None

        try:
            jsoned = res.json()
            return jsoned
        except Exception as e:
            self.log(str(e))
            return False

    def get_unix_timestamp(self):
        return memes.lib.utils.get_current_unix()

    def download_thumb(self, thumb_path, tim, thread, post):
        pass

    def get_thread_url(self, board, no):
        return "{}{}/res/{}.html".format(self.url, board, no)

    def get_next_thread(self):

        sleep(1)


        #thread = memes.models.Thread.objects.filter(source=self.name, is_locked=0).order_by("last_check_time").first()

        current_unix = memes.lib.utils.get_current_unix()
        thread = memes.models.Thread.objects.filter(source=self.name, is_locked=0, last_check_time__lte=current_unix-self.required_post_age).order_by("last_check_time").first()
        return thread


    def get_alive_thread_count(self):
        return memes.models.Thread.objects.filter(source=self.name, is_locked=0).count()

    def fetch_catalog(self):
        pass

    def fetch_thread(self, thread):
        pass

    def syck(self):
        pass

    def get_thread(self, no, board):
        return memes.models.Thread.objects.filter(source=self.name, board=board, no=no).first()

    def get_threads_post(self, thread, no):
        return memes.models.Post.objects.filter(thread=thread, no=no).first()

    # Check if post is already added to db, first from memory to filter out most of db queries, and then from db.
    def post_handler_check_if_added(self, post, thread, num_key_name=None):
        """
        if not num_key_name:
            raise ValueError('no_key_name is not set')

        post_key = str(post[num_key_name]) + thread.board

        if post_key in self.handled_posts:
            return True
        self.handled_posts.append(post_key)

        if len(self.handled_posts) > 25000 * len(self.boards):
            self.handled_posts.pop(0)
        """
        is_dub = bool(self.get_threads_post(thread, post[num_key_name]))

        return is_dub

    def make_file(self, tim, file_url, post_data, thread):

        #temp_f = tempfile.NamedTemporaryFile()

        tmp_file_path =  "/tmp/" + str(uuid.uuid4())

        memes.lib.files.download_to_path(file_url, tmp_file_path, http_session=self.http_session)

        if not os.path.isfile(tmp_file_path):
            self.print("Failed to download " + file_url)
            return

        md5 = hashlib.md5(open(tmp_file_path, 'rb').read()).hexdigest()

        file = memes.models.File.objects.filter(md5=md5).first()

        # File has been downloaded before
        if file:
            has_file_content = os.path.isfile(file.get_path()) and os.path.getsize(file.get_path()) > 0

            if not has_file_content:
                if not memes.lib.files.has_enough_space(file.cached_storage):
                    memes.lib.files.remove_till_enough_space(file.cached_storage)
                shutil.copy(tmp_file_path, file.get_path())
                file.content_downloaded = True
                file.save()
            os.remove(tmp_file_path)
        else:
            file = memes.lib.files.add_file(tmp_file_path)
            if not file:
                self.log("Failed to create file from " + file_url)
                os.remove(tmp_file_path)
                return None

        if file:
            file.post_count += 1
            file.added_at = Now()
            file.save()

            thumb_path = file.cached_storage.file_path + "thumbs/" + str(file.id) + ".jpg"

            has_thumb = os.path.isfile(thumb_path) and os.path.getsize(thumb_path) > 0

            if not has_thumb:
                if memes.lib.files.generate_thumb(file.get_path(), thumb_path, file.type):
                    os.chmod(thumb_path, 0o755)
            else:
                os.system("touch " + thumb_path);
                #self.print("Had thumb already for " + str(file.id))

            #os.chmod(file.get_path(), 0o755)

        #temp_f.close()

        return file

    def post_handler(self, post, thread):
        pass

    def get_pph(self):
        return 0
        h_ago = timezone.now() - timezone.timedelta(hours=1)


        return memes.models.Post.objects.filter(thread__source=self.name, thread__is_locked=False, added_at__gte=h_ago).count()

    def run(self):

        self.thread_status = 1

        while self.thread_status == 1:
            try:
                self.syck()
            except Exception as e:
                self.log(str(e))

                import django.db; django.db.close_old_connections()

                import traceback
                traceback_str = ''.join(traceback.format_tb(e.__traceback__))
                self.log(str(traceback_str))
                sleep(5)
                continue

        self.thread_status = 0
        return 0


    def stop(self):
        self.thread_status = -1

    def is_running(self):
        return self.thread_status == 1

    def print(self, message):
        return print("{}: {}".format(self.name, message))

    def log(self, message):
        print_output = "{}: {}".format(type, message)
        #self.print(print_output)

        time = strftime("%d.%m.%Y %H:%M:%S", gmtime())
        log_output = "{} - {}".format(time, print_output)

        log_path = "logs/monitor_{}.txt".format(self.name)

        with open(log_path, 'a+') as out:
            out.write(log_output + "\n")
