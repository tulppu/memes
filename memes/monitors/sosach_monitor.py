import memes.models
import memes.lib.files
import memes.lib.files
import memes.monitors.base_monitor
import memes.lib.utils
from time import sleep
import requests.exceptions
from memes.signals import post_fully_built

class SosachMonitor(memes.monitors.base_monitor.BaseMonitor):

    url = "https://2ch.hk/"
    name = '2ch'
    boards = ["int", "b", "wm", "h", "po", "e"]
    time_zone = 'Europe/Moscow'
    required_post_age = 60 * 60 * 2
    html_to_text = True
    auto_startup = True 
    http_session = requests.Session()

    def monitor_threads(self):

        for board in self.boards:

            uri = "https://2ch.hk/" + board + "/catalog.json"
            res = self.get_json(uri)

            if not res:
                self.print("Failed to fetch " + uri)
                continue

            for thread in res["threads"]:
                obj = self.get_thread(thread["num"], board)

                if not obj:
                    obj = memes.models.Thread()
                    obj.no = thread["num"]
                    obj.board = board
                    obj.source = self.name
                    obj.timestamp = thread["timestamp"]
                    obj.last_check_time = thread["timestamp"]
                    #obj.last_check_time = 0;
                    obj.subject = thread["subject"] if "subject" in thread else None
                    obj.save()

                #if obj not in self.monitored_threads:
                #    self.monitored_threads.append(obj)

                #if len(self.monitored_threads) > 250 * len(self.boards):
                #    self.monitored_threads.pop()

    def fetch_thread(self, thread):
        return self.get_json("https://2ch.hk/" + thread.board + "/res/" + str(thread.no) + ".json", last_modified=thread.last_check_time)

    def syck(self):

        last_catalog_fetch = 0

        while self.thread_status == 1:

            if memes.lib.utils.get_current_unix() > last_catalog_fetch + 60:
                self.monitor_threads()
                last_catalog_fetch = memes.lib.utils.get_current_unix()

            thread = self.get_next_thread()

            if not thread:
                sleep(30)
                continue

            try:
                thread_data = self.fetch_thread(thread)
            except requests.exceptions.ConnectionError:
                continue
            finally:
                thread.last_check_time = memes.lib.utils.get_current_unix()
                thread.save()

            if thread_data is False:
                continue

            if thread_data is None:
                self.print("Closing thread {} @ {}/{}".format(thread.no, self.name, thread.board))
                thread.is_locked = True
                thread.save()
                continue

            posts = thread_data["threads"][0]["posts"]

            if posts:
                for post in posts:

                    if self.thread_status == -1:
                        break

                    if self.required_post_age and post["timestamp"] + self.required_post_age > self.get_unix_timestamp():
                        break

                    if self.post_handler_check_if_added(post, thread):
                        continue
                    self.post_handler(post, thread)


    def post_handler_check_if_added(self, post, thread, num_key_name="num"):
        return super(SosachMonitor, self).post_handler_check_if_added(post, thread, num_key_name)

    def post_handler(self, post_data, thread):

        if "files" not in post_data or not post_data["files"]:
            return

        def make_post(data):
            post = memes.models.Post()

            post.source = self.name
            post.thread = thread
            post.no = data['num']
            post.message = data['comment'] if "comment" in data else None
            post.country = None
            post.timestamp = data['timestamp']

            if post.no == thread.no:
                post.op = 1
            
            try:
                post.save()
            except Exception:
                return None;
                

            return post

        post = make_post(post_data)

        for file_data in post_data["files"]:

            if self.thread_status == -1:
                break

            url = "https://2ch.hk" + file_data["path"]
            file = self.make_file(post_data['num'], url, post_data, thread,)

            if not file:
                continue

            post_file = memes.models.PostFile()
            post_file.file_name = file_data['fullname'] if "fullname" in file_data else file_data["displayname"]
            post_file.file = file
            post_file.post = post
            post_file.orig_url = url

            post_file.save()

            #sleep(1)
        #memes.signals.post_fully_built.send(sender=memes.models.Post, post=post)
