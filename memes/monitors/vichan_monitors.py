import memes.monitors.vichan
from time import sleep

"""
class NcMonitor(memes.monitors.vichan.ViMonitor):

    name = "NC"
    url = "http://nyymichan.fi/"
    boards = ["b", "flode"]
    time_zone = 'Europe/Helsinki'
    required_post_age = 60 * 5
    default_ext = ".gif"
    auto_startup = True
    archive_postless = True

    #def get_thumb_url(self, thread, tim, post):
    #    return self.url + thread.board + "/thumb/" + tim + self.get_thumb_ext(post["ext"])
"""
"""
class KohlMonitor(memes.monitors.vichan.ViMonitor):

    name = "Kohl"
    url = "https://kohlchan.net/"
    timezone = "Europe/Berlin"
    boards = ["int", "b"]
    default_ext = ".png"
    required_post_age = 45 * 60

    def get_thumb_url(self, thread, tim, post):
        return self.url + thread.board + "/thumb/" + tim + self.get_thumb_ext(post["ext"])
"""

"""
class InfMonitor(memes.monitors.vichan.ViMonitor):

    name = "8ch"
    url = "https://8ch.net/"
    boards = ["int", "kc", "pol", "leftypol", "v", "a", "r9k", "rule34", "co"]
    time_zone = 'America/New_York'
    default_ext = ".jpg"
    required_post_age = 60 * 60 * 1.5

    def get_file_url(self, thread, file_name, post):
        return self.url + "file_store/" + file_name

    def get_thumb_url(self, thread, tim, post):
        return self.url + "file_store/thumb/" + tim + self.get_thumb_ext(post["ext"])
"""
"""
class SuomiMonitor(memes.monitors.vichan.ViMonitor):

    name = "Suomilauta"
    url = "https://suomilauta.org/"
    boards = ["b", "a", "fap", "he", "rr"]
    default_ext = ".png"
    required_post_age = 45 * 60
    auto_startup = True
    archive_postless = True

    def get_thumb_url(self, thread, tim, post):
        return self.url + thread.board + "/thumb/" + tim + self.get_thumb_ext(post["ext"])
"""

class MintMonitor(memes.monitors.vichan.ViMonitor):
    name = "Mint"
    url = "https://mintboard.org/vichan/"
    boards = ["bone"]
    default_ext = ".png"
    required_post_age = 45 * 60
    auto_startup = True

    #def get_thumb_url(self, thread, tim, post):
    #    return self.url + thread.board + "/thumb/" + tim + self.get_thumb_ext(post["ext"])
"""
class BRChan(memes.monitors.vichan.ViMonitor):
    name = "55chan"
    url = "https://55chan.org/"
    boards = ["b", "int"]
    default_ext = ".jpg"
    auto_startup = True

    def get_file_url(self, thread, file_name, post_data):
        sleep(5)
        return self.url + thread.board + "/src/" + post_data["tim"] + post_data["ext"]
"""

class NiChan(memes.monitors.vichan.ViMonitor):
    name = "NiChan"
    url = "https://nichan.net/"
    default_ext = ".gif"
    boards = ["a", "b", "pol", "int"]
    auto_startup = True


class PtChan(memes.monitors.vichan.ViMonitor):
    name = "PtChan"
    url = "https://ptchan.org/"
    default_ext = ".jpg"
    boards = ["br", "int" "int", "a"]
    auto_startup = True
    required_post_age = 60 * 90

class TwoTwoChan(memes.monitors.vichan.ViMonitor):
    name = "22Chan"
    url = "https://22chan.org/"
    default_ext = ".jpg"
    boards = ["b", "sewers", "mu"]
    auto_startup = True
    required_post_age = 60 * 90

class WiredSeven(memes.monitors.vichan.ViMonitor):
    name = "W-7"
    url = "https://wired-7.org/"
    default_ext = ".png"
    auto_startup = True
    boards = ["b", "h", "hum", "i", "a", "jp", "mu", "tech", "lain"]
    required_post_age = 60 * 90


class tvch(memes.monitors.vichan.ViMonitor):
    name = "tvch"
    url = "https://tvch.moe/"
    boards = ["tv", "art", "dunk"]
    required_post_age = 60 * 60 * 6
    auto_startup = True
    default_ext = ".png"


class WizardChan(memes.monitors.vichan.ViMonitor):
    name = "Wizardchan"
    url = "https://wizchan.org/"
    boards = ["wiz", "dep", "hob", "lounge", "jp"]
    required_post_age = 60 * 60 * 6
    auto_startup = True
    default_ext = ".jpg"

class Seksilauta(memes.monitors.vichan.ViMonitor):
    name = "Seksilauta"
    url = "https://seksilauta.com/"
    required_post_age = 60 * 60 * 6
    boards = ["julkkis", "milf", "teinit", "onlyfans", "some", "suomiporno", "intporno", "suomi", "int", "ansat", "hentai"]
    auto_startup = True
    default_ext = ".png"


class Mintboard(memes.monitors.vichan.ViMonitor):
    name = "Mintboard"
    url = "https://mintboard.org/vichan/"
    required_post_age = 60 * 30
    boards = ["bone", "bitch"]
    auto_startup = True
    default_ext = ".png"

class Loistolauta(memes.monitors.vichan.ViMonitor):
    name = "Loistolauta"
    url = "https://loistolauta.org/"
    required_post_age = 60 * 30
    boards = ["b"]
    auto_startup = True
    default_ext = ".png"
    archive_postless = True


# not vichan api
class MlPol(memes.monitors.vichan.ViMonitor):
    name = "mlpol"
    url = "https://mlpol.net/"
    boards = ["mlpol", "qa", "1intr", "vx", "cyb", "a"]
    required_post_age = 60 * 60 * 6
    auto_startup = True
    default_ext = ".png"
