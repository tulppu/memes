import memes.monitors.base_monitor
import memes.lib.utils
import memes.lib.files
import memes.models
from bs4 import BeautifulSoup
from time import sleep
import asyncio
import uuid
import hashlib
import os
import shutil


class YliMonitor(memes.monitors.base_monitor.BaseMonitor):
    name = 'Ylilauta'
    url = 'https://ylilauta.org/'
    boards = ['satunnainen', 'sekalainen', 'international', 'ohjelmointi', 'yhteiskunta']
    time_zone = 'Europe/Helsinki'

    loop = None

    def stop(self):
        if self.loop and self.loop.is_running():
            self.loop.stop()
            self.loop.close()
        else:
            print("Loop was not running")
        return super(YliMonitor, self).stop()

    def get_threads_url(self, board):
        return "https://ylilauta.org/" + board + "/"

    def get_html(self, uri):
        futures = [memes.lib.utils.download_hml(uri)]

        if not self.loop:
            self.loop = asyncio.new_event_loop()
            asyncio.set_event_loop(self.loop)
            print("loop set")

        return self.loop.run_until_complete(asyncio.gather(*futures))[0]

    def monitor_threads(self):
        for board in self.boards:
            html = self.get_html(self.get_threads_url(board))
            if not html:
                print("Failed to fetch " + self.get_threads_url(board))
                return

            soup = BeautifulSoup(html, "lxml")
            threads = soup.findAll('div', {'class': 'thread'})
            if not threads:
                return
            for thread in threads:
                no = thread.get("id")[7:]

                obj = self.get_thread(no, board)

                if not obj:
                    obj = memes.models.Thread()
                    obj.no = no
                    obj.board = board
                    obj.source = self.name
                    sub = thread.find("span", {"class": "subject"}).getText()
                    if sub and len(sub):
                        sub = memes.lib.utils.remove_emojii(sub)
                        print(sub)
                    obj.subject = sub

                    date = thread.find("time").getText()

                    obj.timestamp = memes.lib.utils.str_date_to_unix(date, self.time_zone)
                    obj.save()

                if obj not in self.monitored_threads:
                    self.monitored_threads.append(obj)

    def fetch_thread(self, thread):
        return self.get_html("https://ylilauta.fi/" + thread.board + "/" + str(thread.no))

    def syck(self):
        last_threads_fetch = 0

        while True:
            if memes.lib.utils.get_current_unix() > last_threads_fetch + 30:
                self.monitor_threads()
                last_threads_fetch = memes.lib.utils.get_current_unix()

            thread = self.get_next_thread()

            if not thread:
                print("No thread found")
                self.monitor_threads()
                sleep(30)
                continue

            thread_html = self.fetch_thread(thread)

            soup = BeautifulSoup(thread_html, "lxml")
            posts = soup.find_all('div', {'class': 'op_post'}) + soup.find_all('div', {'class': 'answer'})

            for post in posts:
                # print("Handling post " + post)
                # if self.required_post_age and post["time"] + self.required_post_age > self.get_unix_timestamp():
                #    thread_can_be_closed = False
                #    break

                self.post_handler(post, thread)

    def post_handler(self, post, thread):

        img_el = post.find("a", {"class": "expandlink"})
        embed_el = post.find("a", {"class": "playembedlink"})

        file_url = None

        if not img_el and not embed_el:
            return

        if img_el and not embed_el:
            file_url = img_el['href']
        else:
            figcaption = post.find("figcaption")
            orig_name_link = figcaption.find("a")
            if orig_name_link and "href" in orig_name_link:
                file_url = orig_name_link["href"]

            if not file_url:
                url = "https://ylilauta.org/scripts/ajax/embedhtml.php?id=" + embed_el["data-embedcode"] \
                      + "&type=" + embed_el["data-embedsource"]
                player_html = self.get_html(url)
                soup = BeautifulSoup(player_html, "lxml")
                source = soup.find("source")
                file_url = source["src"] if source and "src" in source else None

        figure = post.find("figure").find("image") if post.find("figure") else None

        thumb_url = figure["url"] if figure else None

        file = None

        if file_url:
            tmp_file_path = "/tmp/" + str(uuid.uuid4())
            memes.lib.files.download_to_path(file_url, tmp_file_path)

            if not os.path.isfile(tmp_file_path):
                print("Failed to download " + file_url)

            md5 = hashlib.md5(open(tmp_file_path, 'rb').read()).hexdigest()
            file = memes.models.File.objects.filter(md5=md5).first()
            if not file:
                file = memes.lib.files.add_file(tmp_file_path)

            # File was downloaded before but then deleted in order to free space
            if file and (not file.content_downloaded or not os.path.isfile(file.get_path())):
                if not memes.lib.files.has_enough_space(file.cached_storage):
                    memes.lib.files.remove_till_enough_space(file.cached_storage)
                shutil.move(tmp_file_path, file.cached_storage.file_path + file.file_name)
                file.content_downloaded = True
                file.save()

            #if os.path.isfile(tmp_file_path):
            #    os.unlink(tmp_file_path)

            thumb_saved = False
            if thumb_url:
                thumb_dir = file.cached_storage.file_path + "thumbs/" + str(file.id) + ".jpg"
                thumb_saved = bool(memes.lib.files.download_to_path(thumb_url, thumb_dir))

            if not thumb_saved:
                thumb_path = file.cached_storage.file_path + "thumbs/" + str(file.id) + ".jpg"
                memes.lib.files.generate_thumb(file.get_path(), thumb_path, file.type)

        if not file:
            print("no file")

        msg = post.find("div", {'class': 'postcontent'}).getText()

        archived_post = memes.models.Post()

        archived_post.thread = thread
        archived_post.no = post.get("id")[2:]
        msg = memes.lib.utils.remove_emojii(msg)
        archived_post.message = msg if msg and len(msg) else None
        archived_post.country = None

        datetime = post.find("time").getText()

        archived_post.timestamp = memes.lib.utils.str_date_to_unix(datetime, self.time_zone)
        archived_post.data = post.getText()

        archived_post.save()

        if file:
            post_file = memes.models.PostFile()
            post_file.post = archived_post
            post_file.file = file
            post_file.file_name = file_url
            post_file.save()
