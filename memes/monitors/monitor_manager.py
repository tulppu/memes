from memes.monitors.kanker_monitor import KankerMonitor
from memes.monitors.sosach_monitor import SosachMonitor
from memes.monitors.vichan_monitors import *
from memes.monitors.lynx_monitors import *
from memes.monitors.jschan_monitors import *
from memes.monitors.weed_monitor import WeedMonitor

import threading

monitors = [
    KankerMonitor(),
    SosachMonitor(),
    NyymiMonitor(),
    #NcMonitor(),
    #InfMonitor(),
    KohlMonitor(),
    #SuomiMonitor(),
    #SpaceMonitor(),
    #FourKeks(),
    #IndiaMonitor(),
    IndiaMonitor(),
    #BRChan(),
    NiChan(),
    SixteenChan(),
    PtChan(),
    TwoTwoChan(),
    FchMonitor(),
    WiredSeven(),
    EndMonitor(),
    BunkerChan(),
    OneeChan(),
    Waifuist(),
    FinalChan(),
    ProLikeWoah(),
    tvch(),
    AlogSpace(),
    WizardChan(),
    # MlPol(),
    #Seksilauta(),
    Mintboard(),
    Loistolauta(),
    #zzzMonitor()
    #WeedMonitor()
]

threads = {}

import asyncio

def loop_in_thread(monitor):
    asyncio.set_event_loop(monitor.loop)
    monitor.loop.run_until_complete(monitor.run())
    print("Closed " + monitor.name)
    monitor.loop.close()

def start_monitor(monitor):
    monitor.loop = asyncio.new_event_loop()

    import threading
    t = threading.Thread(target=loop_in_thread, args=(monitor,))
    t.start()

    print("Started?")

def stop_monitor(monitor):
    monitor.stop_running()

def start_monitors():
    for monitor in monitors:
        start_monitor(monitor)
        print("Started "  + monitor.name)
        if monitor.get_status() == 1:
            start_monitor(monitor)
            print("Started")
        #if not monitor.is_running():
        #    monitor.run()
    return

def is_running(monitor):
   t = threads[monitor] if monitor in threads else None
   if not t:
       return False
   return t.is_alive() and monitor.get_status() == 1


def get_monitor_by_name(str, iexact=False):
    for monitor in monitors:
        if iexact:
            if monitor.name.lower() == str.lower():
                return monitor
        else:
            if monitor.name == str:
                return monitor
    return None
