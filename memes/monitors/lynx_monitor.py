import memes.monitors.base_monitor
import memes.lib.utils
import memes.lib.files
from time import sleep
import requests.exceptions
from dateutil import parser
import memes.models
from django.db import IntegrityError
from memes.lib.utils import get_current_unix

from memes.signals import post_fully_built

class LynxMonitor(memes.monitors.base_monitor.BaseMonitor):



    monitored_thread_ids = []
    name = None
    auto_startup = False
    archive_postless = False
    verify_cert = True
    url = None
    boards = None
    required_post_age = 60 * 60 * 2
    rest_time = 3


    def make_unix_timestamp(self, date):
        dt = parser.parse(date)
        return int(dt.strftime("%s"))

    def get_catalog_url(self, board):
        return self.url + board + "/catalog.json"

    def get_thread_id(self, board, post_id):
        return board + "_" + post_id

    def fetch_catalog(self):


        for board in self.boards:

            last_fetch = self.board_last_fetch[board] if board in self.board_last_fetch else None

            uri = self.get_catalog_url(board)
            threads = self.get_json(uri, last_fetch)
            self.board_last_fetch[board] = memes.lib.utils.get_current_unix()

            if not threads or not len(threads):
                sleep(30)
                continue

            sleep(self.rest_time)

            for thread in threads:

                # TODO: properly use monitored_threads
                obj = self.get_thread(thread["threadId"], board)

                # revive
                if obj and obj.is_locked is True:
                    obj.is_locked = False
                    obj.save()

                if not obj:
                    obj = memes.models.Thread()
                    obj.no = thread["threadId"]
                    obj.subject = thread["subject"]
                    obj.board = board
                    obj.source = self.name
                    obj.timestamp = self.make_unix_timestamp(thread["creation"]) if "creation" in thread else get_current_unix()
                    obj.last_check_time = obj.timestamp
                    obj.save()

                if obj not in self.monitored_threads:
                    self.monitored_threads.append(obj)

                if len(self.monitored_threads) > 500 * len(self.boards):
                    self.monitored_threads.pop()

    def get_thread_url(self, board, no, extension="html"):
        return self.url + board + "/res/" + str(no) + "." + extension

    def fetch_thread(self, thread):
        url = self.get_thread_url(thread.board, thread.no, "json")
        return self.get_json(url, last_modified=thread.last_check_time)

    def syck(self):

        last_catalog_fetch = 0

        while self.thread_status == 1:
            if memes.lib.utils.get_current_unix() > last_catalog_fetch + 60 * 3:
                self.fetch_catalog()
                last_catalog_fetch = memes.lib.utils.get_current_unix()

            thread = self.get_next_thread()

            if not thread:
                self.print("Thread not found to be fetched")
                sleep(5)
                continue

            try:
                thread_data = self.fetch_thread(thread)
                sleep(self.rest_time)
            except requests.exceptions.ConnectionError:
                continue
            finally:
                thread.last_check_time = memes.lib.utils.get_current_unix()
                thread.save()

            if thread_data is False:
                continue

            if thread_data is None:
                self.print("Closing thread {} @ {}/{}".format(thread.no, self.name, thread.board))
                thread.is_locked = True
                thread.save()
                #if not thread.posts.count():
                    #thread.delete()
                    #print("deleted postless thread " + str(thread.no))
                continue


            posts = thread_data["posts"]

            thread_timestamp = thread.timestamp

            if self.required_post_age and thread.timestamp + self.required_post_age > self.get_unix_timestamp():
                self.print("Skipping thread {} @ {}/{} since it's too fresh".format(thread.no, self.name, thread.board))
                continue

            if not self.check_if_dub(thread_data, thread):
                post = self.post_handler(thread_data, thread)
                #if post:
                #    post.op = 1
                    #post.save()


            if posts:
                for post in posts:

                    if self.thread_status == -1:
                        break

                    post_timestamp = self.make_unix_timestamp(post["creation"])
                    #if self.required_post_age and post_timestamp + self.required_post_age > self.get_unix_timestamp():
                    #    break

                    if not self.archive_postless and (not "files" in post):
                        continue

                    if self.check_if_dub(post, thread):
                        continue

                    self.post_handler(post, thread)
        self.thread_status = 0

    def check_if_dub(self, post, thread):

        post_no = post["postId"] if "postId" in post else post["threadId"]

        #post_key = str(post_no) + thread.board + "/" + self.source
        post_key = str(post_no) + thread.board + "/" + self.name


        if post_key in self.handled_posts:
            return True
        self.handled_posts.append(post_key)

        if len(self.handled_posts) > 150000 * len(self.boards):
            self.handled_posts.pop(0)

        exists = memes.models.Post.objects.filter(no=post_no, thread_id=thread.id).count() == 1

        return exists
        #is_dub = bool(self.get_threads_post(thread, post[no_key_name]))
        #return is_dub


    def post_handler(self, post_data, thread):

            if ("files" not in post_data or not post_data["files"] ) and not self.archive_postless:
                return

            post_no = post_data["postId"] if "postId" in post_data else post_data["threadId"]

            def make_post(data):
                obj = memes.models.Post()

                obj.source = self.name
                obj.thread = thread
                obj.no = post_no
                obj.message = data['message'] if "message" in data else None
                obj.country = data["flagCode"] if "flagCode" in data else None
                obj.timestamp = self.make_unix_timestamp(post_data["creation"])

                if obj.no == thread.no:
                    obj.op = 1

                obj.save()

                return obj

            post = make_post(post_data)

            # Avoid duplicate files from same post.
            handled_files = []

            for file_data in post_data["files"]:

                if file_data["path"] in handled_files:
                    continue
                handled_files.append(file_data["path"])

                url = self.url[:-1] + file_data["path"]
                file = self.make_file(post_no, url, post_data, thread)
                sleep(self.rest_time)

                if not file:
                    continue

                post_file = memes.models.PostFile()
                post_file.file_name = file_data['originalName']
                post_file.file = file
                post_file.post = post
                post_file.orig_url = url

                post_file.save()

            #memes.signals.post_fully_built.send(sender=memes.models.Post, post=post)


            return post
