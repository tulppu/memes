import memes.monitors.lynx_monitor


"""
class SpaceMonitor(memes.monitors.lynx_monitor.LynxMonitor):

    name = "Space"
    url = "https://spacechan.xyz/"
    boards = ["b"]
    required_post_age = 60 * 30
"""


class KohlMonitor(memes.monitors.lynx_monitor.LynxMonitor):

    name = "KohlLynx"
    url = "https://kohlchan.net/"
    boards = ["int", "b"]
    required_post_age = 60 * 60 * 0.45 
    auto_startup = True 
    archive_postless = True
    rest_time = 1


class NyymiMonitor(memes.monitors.lynx_monitor.LynxMonitor):

    name = "Nyymichan"
    url = "https://nyymichan.fi/"
    boards = ["b", "g", "2022", "a", "h", "hikky"]
    required_post_age = 60 * 10
    auto_startup = True 
    archive_postless = True



class FourKeks(memes.monitors.lynx_monitor.LynxMonitor):

    name = "4keks"
    url = "https://4keks.org/"
    boards = ["4keks"]
    required_post_age = 60 * 45


class IndiaMonitor(memes.monitors.lynx_monitor.LynxMonitor):

    name = "IndiaChan"
    url = "https://indiachan.com/"
    boards = ["a", "b", "g", "pol", "ent"]
    required_post_age = 60 * 60
    auto_startup = True
    verify_cert = False

class SixteenChan(memes.monitors.lynx_monitor.LynxMonitor):

    name = "16chan"
    url = "https://16chan.xyz/"
    boards = ["b", "pol"]
    required_post_age = 60 * 60 * 2
    auto_startup = True

class FchMonitor(memes.monitors.lynx_monitor.LynxMonitor):

    name = "Fch"
    url = "https://fch.bet/"
    boards = ["b", "int", "wp", "s"]
    required_post_age = 60 * 60 * 2
    auto_startup = True

class EndMonitor(memes.monitors.lynx_monitor.LynxMonitor):

    name = "Endchan"
    url = "https://endchan.net/"
    boards = ["ausneets", "yuri", "kc", "b", "bbg", "mouto"]
    required_post_age = 60 * 60 * 2
    auto_startup = True


class BunkerChan(memes.monitors.lynx_monitor.LynxMonitor):

    name = "Bunkerchan"
    url = "https://bunkerchan.xyz/"
    boards = ["leftypol", "hobby", "anime", "tech"]
    required_post_age = 60 * 60 * 3
    auto_startup = True

class OneeChan(memes.monitors.lynx_monitor.LynxMonitor):
    name = "OneeChan"
    url = "https://onee.ch/"
    boards = ["b", "pol", "biz", "cyb"]
    required_post_age = 60 * 60 * 1
    auto_startup = True
    verify_cert = False

class Waifuist(memes.monitors.lynx_monitor.LynxMonitor):
    name = "Waifuist"
    url = "https://waifuist.pro/"
    boards = ["w"]
    required_post_age = 60 * 60 * 1
    auto_startup = True

class FinalChan(memes.monitors.lynx_monitor.LynxMonitor):
    name = "Finalchan"
    url = "https://finalchan.net/"
    boards = ["r"]
    required_post_age = 60 * 60 * 1
    auto_startup = True

class ProLikeWoah(memes.monitors.lynx_monitor.LynxMonitor):
    name = "ProLikeWoah"
    url = "https://prolikewoah.com/"
    boards = ["hgg", "animu"]
    required_post_age = 60 * 60 * 1
    auto_startup = True


class AnonCafe(memes.monitors.lynx_monitor.LynxMonitor):
    name = "AnonCafe"
    url = "https://anon.cafe/"
    boards = ["fascist", "k", "comfy"]
    required_post_age = 60 * 60 * 1
    auto_startup = True


class AlogSpace(memes.monitors.lynx_monitor.LynxMonitor):
    name = "AlogSpace"
    url = "https://alogs.theguntretort.com/"
    boards = ["cow", "ita", "sw", "mu"]
    required_post_age = 60 * 60 * 1
    auto_startup = True

class Asherahsgarden(memes.monitors.lynx_monitor.LynxMonitor):
    name ="AsherahsGarden"
    url = "https://asherahsgarden.net/"
    boards = ["ft", "am", "si", "ot"]
    required_post_age = 60 * 60 * 1
    auto_startup = True
    archive_postless = True
