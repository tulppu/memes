import django.dispatch


post_fully_built = django.dispatch.Signal(providing_args=["post"])
